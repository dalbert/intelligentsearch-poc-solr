# README #

### Setup ###

1. Start SOLR 

    *(From the root of the project)*:

    ``` 
    ./bin/solr start
    ```

1. Index Products, Customers, and Orders 

    ```
    cd example/architechsample 
    ```

    ``` 
    ./index_customers.sh
    ```

    ```
    ./index_products.sh
    ```

    ```
    ./index_orders.sh
    ```

### Shutdown ###

``` 
./bin/solr stop
```